-- Your SQL goes here
CREATE TABLE drink_requests (
    id SERIAL PRIMARY KEY,
    dispenser_id INT NOT NULL,
    drink_id INT NOT NULL,
    price NUMERIC NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    expires_at TIMESTAMP NOT NULL,
    volume INT DEFAULT 0
)