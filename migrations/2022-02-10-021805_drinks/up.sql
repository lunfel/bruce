-- Your SQL goes here

CREATE TABLE drinks (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    group_id INT NOT NULL,
    price NUMERIC NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_group_id
        FOREIGN KEY(group_id)
            REFERENCES groups(id)
)