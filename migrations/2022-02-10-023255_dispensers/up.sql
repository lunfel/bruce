-- Your SQL goes here

CREATE TABLE dispensers (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    group_id INT NOT NULL,
    drink_id INT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_group_id
        FOREIGN KEY(group_id)
            REFERENCES groups(id),
    CONSTRAINT fk_drink_id
        FOREIGN KEY(drink_id)
            REFERENCES drinks(id)
);