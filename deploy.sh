#!/usr/bin/env bash

aws --profile bruce-server-cloudformation-service-account cloudformation deploy \
    --region ca-central-1 \
    --template-file ./cloudformation/server.yaml \
    --stack-name bruce-server \
    --capabilities CAPABILITY_IAM \
    --capabilities CAPABILITY_NAMED_IAM