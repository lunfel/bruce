-- psql -U postgres -h 127.0.0.1 -d bruce -f seeds/init.sql

INSERT INTO users(first_name, last_name, email, password) VALUES ('Mathieu', 'Tanguay', 'mathieu.tanguay871@gmail.com', 'secret123');
INSERT INTO groups(name) VALUES ('Beauharnois Beach House');
INSERT INTO drinks(name, group_id, price) VALUES ('Biere', 1, 0.1),
    ('Rhum and Coke', 1, 0.3),
    ('Vin Rouge', 1, 0.25);

INSERT INTO dispensers(name, group_id, drink_id) VALUES ('Alfred', 1, 3);
INSERT INTO group_user(group_id, user_id) VALUES (1, 1);
