use diesel::result::Error as DieselError;
use rocket::response::{self, Responder};
use rocket::Request;
use rocket::http::Status;

#[derive(Debug)]
pub struct LoginError(DieselError);

impl From<DieselError> for LoginError {
    fn from(error: DieselError) -> Self {
        LoginError(error)
    }
}

impl<'r> Responder<'r, 'static> for LoginError {
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'static> {
        response::Response::build().status(Status::Forbidden).ok()
    }
}
