use bigdecimal::BigDecimal;
use chrono::NaiveDateTime;
use crate::schema::drink_requests;

#[derive(Deserialize, Insertable)]
#[table_name = "drink_requests"]
pub struct NewDrinkRequestForm {
    pub dispenser_id: i32,
    pub drink_id: i32,
    pub price: BigDecimal,
    pub created_at: NaiveDateTime,
    pub expires_at: NaiveDateTime
}

#[derive(Deserialize, Insertable)]
#[serde(crate = "rocket::serde")]
#[table_name = "drink_requests"]
pub struct UpdateVolumeRequestForm {
    pub volume: i32
}
