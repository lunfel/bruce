use rocket::{Request, response};
use rocket::http::Status;
use rocket::response::Responder;
use rocket_jwt::jwt;

static SECRET_KEY: &str = "secret_key";

#[jwt(SECRET_KEY)]
pub struct UserClaim {
    pub id: String
}

#[derive(Deserialize, Serialize)]
pub struct UserTokenResponse {
    pub token: String
}
