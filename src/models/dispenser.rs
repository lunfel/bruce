use chrono::NaiveDateTime;
use rocket::request::FromParam;
use crate::orm::establish_connection;
use crate::schema::dispensers::dsl::*;
use diesel::prelude::*;
use std::option::Option;

#[derive(Queryable, Serialize)]
pub struct Dispenser {
    pub id: i32,
    pub name: String,
    pub group_id: i32,
    pub drink_id: Option<i32>,
    pub created_at: NaiveDateTime
}

impl<'a> FromParam<'a> for Dispenser {
    type Error = diesel::result::Error;
    fn from_param(dispenser_id: &'a str) -> Result<Self, Self::Error> {
        let conn = establish_connection();

        let dispenser_id: i32 = dispenser_id.parse().unwrap();

        dispensers.find(dispenser_id)
            .first::<Dispenser>(&conn)
    }
}

#[derive(Queryable, Serialize)]
pub struct DispenserWithDrink {
    pub id: i32,
    pub name: String,
    pub group_id: i32,
    pub drink_id: i32,
    pub created_at: NaiveDateTime
}

impl<'a> FromParam<'a> for DispenserWithDrink {
    type Error = diesel::result::Error;
    fn from_param(dispenser_id: &'a str) -> Result<Self, Self::Error> {
        let conn = establish_connection();

        let dispenser_id: i32 = dispenser_id.parse().unwrap();

        let d = dispensers.find(dispenser_id)
            .first::<Dispenser>(&conn);

        d.map_or(Err(diesel::result::Error::NotFound), |d| d.try_into().map_err(|_| diesel::result::Error::NotFound))
    }
}

pub enum DispenserError {
    WithoutDrink
}

impl TryFrom<Dispenser> for DispenserWithDrink {
    type Error = DispenserError;
    fn try_from(dispenser: Dispenser) -> Result<Self, Self::Error> {
        if dispenser.drink_id.is_none() {
            Err(DispenserError::WithoutDrink)
        } else {
            Ok(DispenserWithDrink {
                id: dispenser.id,
                name: dispenser.name,
                group_id: dispenser.group_id,
                drink_id: dispenser.drink_id.unwrap(),
                created_at: dispenser.created_at
            })
        }
    }
}
