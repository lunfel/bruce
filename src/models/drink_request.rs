use bigdecimal::BigDecimal;
use chrono::{Local, NaiveDateTime};
use crate::schema::drink_requests;
use rocket::request::FromParam;
use crate::orm::establish_connection;
use crate::schema::drink_requests::dsl::*;
use diesel::prelude::*;

#[derive(Deserialize, Insertable, Serialize, Queryable)]
pub struct DrinkRequest {
    pub id: i32,
    pub dispenser_id: i32,
    pub drink_id: i32,
    pub price: BigDecimal,
    pub created_at: NaiveDateTime,
    pub expires_at: NaiveDateTime,
    pub volume: Option<i32>
}

impl DrinkRequest {
    pub fn get(model_id: i32) -> QueryResult<Self> {
        let conn = establish_connection();

        drink_requests.find(model_id)
            .first::<DrinkRequest>(&conn)
    }
}

pub fn is_expired(drink_request: &DrinkRequest) -> bool {
    drink_request.expires_at.lt(&Local::now().naive_local())
}

impl<'a> FromParam<'a> for DrinkRequest {
    type Error = diesel::result::Error;
    fn from_param(drink_request_id: &'a str) -> Result<Self, Self::Error> {
        let drink_request_id: i32 = drink_request_id.parse().unwrap();

        DrinkRequest::get(drink_request_id)
    }
}
