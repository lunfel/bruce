use chrono::NaiveDateTime;
use rocket::request::FromParam;
use diesel::prelude::*;
use crate::orm::establish_connection;
use std::result::Result;
use crate::schema::drinks::dsl::*;
use bigdecimal::BigDecimal;

#[derive(Queryable, Serialize)]
pub struct Drink {
    pub id: i32,
    pub name: String,
    pub group_id: i32,
    pub price: BigDecimal,
    pub created_at: NaiveDateTime
}

impl Drink {
    pub fn get(drink_id: &i32) -> Result<Drink, diesel::result::Error> {
        let conn = establish_connection();

        drinks.find(drink_id)
            .first::<Drink>(&conn)
    }
    pub fn all() -> Result<Vec<Drink>, diesel::result::Error> {
        let conn = establish_connection();

        drinks.load(&conn)
    }
}

impl<'a> FromParam<'a> for Drink {
    type Error = diesel::result::Error;
    fn from_param(drink_id: &'a str) -> Result<Self, Self::Error> {
        let drink_id: i32 = drink_id.parse().unwrap();

        Drink::get(&drink_id)
    }
}
