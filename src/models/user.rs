use chrono::NaiveDateTime;
use crate::orm::establish_connection;
use crate::diesel::prelude::*;
use crate::schema::users::dsl::*;
use rocket::request::FromParam;
use std::result::Result;
use crate::errors::login::LoginError;
use crate::forms::login_form::LoginForm;
use crate::security::UserClaim;

#[derive(Queryable, Serialize)]
pub struct User {
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub password: String,
    pub created_at: NaiveDateTime
}

impl User {
    pub fn login(credentials: &LoginForm) -> Result<User, LoginError> {
        let conn = establish_connection();

        users.filter(email.eq(&credentials.username))
            .filter(password.eq(&credentials.password))
            .first::<User>(&conn)
            .map_err(LoginError::from)
    }
}

impl<'a> FromParam<'a> for User {
    type Error = diesel::result::Error;
    fn from_param(user_id: &'a str) -> Result<Self, Self::Error> {
        let conn = establish_connection();

        let user_id: i32 = user_id.parse().unwrap();

        users.find(user_id)
            .first::<User>(&conn)
    }
}

impl From<User> for UserClaim {
    fn from(value: User) -> Self {
        UserClaim {
            id: value.email.to_owned()
        }
    }
}
