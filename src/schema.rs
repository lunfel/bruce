table! {
    dispensers (id) {
        id -> Int4,
        name -> Varchar,
        group_id -> Int4,
        drink_id -> Nullable<Int4>,
        created_at -> Timestamp,
    }
}

table! {
    drink_requests (id) {
        id -> Int4,
        dispenser_id -> Int4,
        drink_id -> Int4,
        price -> Numeric,
        created_at -> Timestamp,
        expires_at -> Timestamp,
        volume -> Nullable<Int4>,
    }
}

table! {
    drinks (id) {
        id -> Int4,
        name -> Varchar,
        group_id -> Int4,
        price -> Numeric,
        created_at -> Timestamp,
    }
}

table! {
    group_user (id) {
        id -> Int4,
        group_id -> Int4,
        user_id -> Int4,
        created_at -> Timestamp,
    }
}

table! {
    groups (id) {
        id -> Int4,
        name -> Varchar,
        created_at -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Int4,
        first_name -> Varchar,
        last_name -> Varchar,
        email -> Varchar,
        password -> Varchar,
        created_at -> Timestamp,
    }
}

joinable!(dispensers -> drinks (drink_id));
joinable!(group_user -> users (user_id));

allow_tables_to_appear_in_same_query!(
    dispensers,
    drink_requests,
    drinks,
    group_user,
    groups,
    users,
);
