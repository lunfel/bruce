use rocket::request::{Request, FromRequest, Outcome};
use crate::schema::drink_requests::dsl::*;
use diesel::prelude::*;
use rocket::http::Status;
use crate::{orm};
use crate::models::drink_request::{DrinkRequest, is_expired};
use orm::establish_connection;

pub struct ActiveDrinkRequest(pub DrinkRequest);

#[derive(Debug)]
pub enum DrinkRequestError {
    Error,
    Expired
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for ActiveDrinkRequest {
    type Error = DrinkRequestError;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let segment = request.routed_segments(0..)
            .enumerate()
            .find(|(_, seg)| seg == &"drink-requests");

        let (n, _value) = match segment {
            Some(content) => content,
            None => {
                return Outcome::Failure((Status::InternalServerError, DrinkRequestError::Error));
            }
        };

        let drink_request_id: i32 = match request.routed_segment(n + 1).unwrap().parse::<i32>() {
            Ok(segment) => segment,
            Err(_) => {
                return Outcome::Failure((Status::InternalServerError, DrinkRequestError::Error));
            }
        };

        let conn = establish_connection();

        let opt_drink_request = drink_requests.find(drink_request_id)
            .first::<DrinkRequest>(&conn);

        let drink_request = match opt_drink_request {
            Ok(drink_request) => drink_request,
            Err(_) => {
                return Outcome::Failure((Status::NotFound, DrinkRequestError::Error))
            }
        };

        match is_expired(&drink_request) {
            true => Outcome::Failure((Status::Forbidden, DrinkRequestError::Expired)),
            false => Outcome::Success(ActiveDrinkRequest(drink_request))
        }
    }
}
