#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel;
// extern crate dotenv;
#[macro_use]
extern crate serde;
extern crate bigdecimal;
extern crate num_bigint;

pub mod errors;
pub mod schema;
pub mod models;
pub mod orm;
pub mod responders;
pub mod forms;
pub mod request_guards;
pub mod fairings;
pub mod security;

use rocket::serde::json::Json;
use models::dispenser::DispenserWithDrink;
use models::dispenser::Dispenser;
use rocket::response::status::Forbidden;
use models::drink::Drink;
use orm::BruceConnPool;
use responders::unprocessable_entity::UnprocessableEntity;
use diesel::insert_into;
use diesel::update;
use forms::drink_requests::NewDrinkRequestForm;
use forms::drink_requests::UpdateVolumeRequestForm;
use chrono::Utc;
use chrono::Duration;
use crate::diesel::RunQueryDsl;
use models::drink_request::DrinkRequest;
use crate::schema::drink_requests::dsl::volume;
use schema::drink_requests::dsl::drink_requests;
use diesel::expression_methods::ExpressionMethods;
use diesel::query_dsl::QueryDsl;
use jsonwebtoken::{Algorithm, TokenData};
use rocket::futures::TryStreamExt;
use crate::errors::login::LoginError;
use crate::fairings::cors::CORS;
use crate::forms::login_form::LoginForm;
use crate::models::user::User;
use crate::request_guards::active_drink_request::ActiveDrinkRequest;
use crate::security::{UserClaim, UserTokenResponse};


#[get("/")]
fn home() -> &'static str {
    "Drink api is up and running!"
}

//noinspection ALL
#[post("/dispensers/<dispenser>/drink-requests", rank = 1)]
async fn drinks(dispenser: DispenserWithDrink, conn: BruceConnPool) -> Result<Json<DrinkRequest>, Forbidden<Option<String>>> {
    use schema::drink_requests::dsl::*;

    let drink = Drink::get(&dispenser.drink_id).expect("Drink not found");

    let drink_request = NewDrinkRequestForm {
        dispenser_id: dispenser.id,
        drink_id: dispenser.drink_id,
        price: drink.price.clone(),
        created_at: Utc::now().naive_utc(),
        expires_at: Utc::now().naive_utc() + Duration::minutes(15)
    };

    let result = conn.run(move |c| insert_into(drink_requests).values(&drink_request).get_result(c)).await.unwrap();

    Ok(Json(result))
}

#[get("/drinks")]
async fn all_drinks(user_claim: UserClaim) -> Result<Json<Vec<Drink>>, Forbidden<Option<String>>> {
    let drinks = Drink::all().expect("Error loading drinks");

    Ok(Json(drinks))
}

#[post("/dispensers/<_dispenser>/drink-requests", rank = 2)]
fn drinks_unprocessable(_dispenser: Dispenser) -> UnprocessableEntity<String> {
    let data = r#"{"message": "The dispenser does not have a drink"}"#;

    UnprocessableEntity(data.into())
}

#[put("/drink-requests/<drink_request>", data = "<form>")]
async fn update_drink_request_volume(mut drink_request: DrinkRequest, form: Json<UpdateVolumeRequestForm>, conn: BruceConnPool, _active_drink_request: ActiveDrinkRequest) -> Result<Json<DrinkRequest>, Forbidden<Option<String>>> {
    let volume_value = form.volume;

    conn.run(move |c| update(drink_requests.find(drink_request.id)).set(volume.eq(volume_value)).get_result::<DrinkRequest>(c)).await.unwrap();

    drink_request.volume = Some(volume_value);

    Ok(Json(drink_request))
}

#[post("/login", data = "<login_form>")]
fn login(login_form: Json<LoginForm>) -> Result<Json<UserTokenResponse>, LoginError> {
    User::login(&login_form).map(|u| Json(UserTokenResponse {
        token: UserClaim::sign(u.into())
    }))
}

#[launch]
fn rocket() -> _ {
    // dotenv().ok();

    // println!("START ENV VARIABLES");
    // for (key, value) in env::vars() {
    //     println!("{}: {}", key, value);
    // }
    // println!("END ENV VARIABLES");

    rocket::build()
        .mount("/", routes![
            drinks,
            all_drinks,
            drinks_unprocessable,
            update_drink_request_volume,
            home,
            login
        ])
        .attach(BruceConnPool::fairing())
        .attach(UserClaim::fairing())
        .attach(CORS)
}
