// #[derive(Responder)]
// #[response(status = 422, content_type = "json")]
// pub struct UnprocessableEntity<'a>(pub &'a str);

use rocket::response::Responder;
use rocket::response;
use rocket::response::Response;
use rocket::request::Request;
// use rocket::response::status::Status;
use rocket::http::Status;

#[derive(Debug, Clone, PartialEq)]
pub struct UnprocessableEntity<R>(pub R);

impl<'r, 'o: 'r, R: Responder<'r, 'o>> Responder<'r, 'o> for UnprocessableEntity<R> {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'o> {
        let mut build = Response::build();
        let responder = self.0;
        
        build.merge(responder.respond_to(req)?);

        build.status(Status::UnprocessableEntity).ok()
    }
}